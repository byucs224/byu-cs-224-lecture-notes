#include <stdio.h>
#include <stdbool.h>

long is_palindrome(long *start, long *end) {
    if (start >= end) {
        return true;
    }
    if (*start != *end) {
        return false;
    }
    return is_palindrome(start + 1, end - 1);
}

int main() {
    long seq[] = {1,2,3,2,1};
    long ans = is_palindrome(seq, seq + 4);
    printf("ans = %ld\n", ans);
    return 0;
}