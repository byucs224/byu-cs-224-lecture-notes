long sum_while(long n) {
    long result = 1;
    long i = 0;
    while (i < n) {
        result += i;
        i++;
    } 
    return result;
}
