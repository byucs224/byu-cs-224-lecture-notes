long square(long rdi);

void main() {
  long rdi = 5;
  long rax = square(rdi); // call square
  return;
}

long square(long rdi) {   //                    square:
  long rax = rdi;         //   long rax = rdi     rrmovq %rdi, %rax       
  long rsi = 1;           //   long rsi = 1       irmovq $1, %rsi
                          //   long rcx = 1;      rrmovq %rsi, %rcx
  while (rsi < rdi) {     // loop:              loop:
                          //   long rdx = rsi     rrmovq %rsi, %rdx
                          //   if (rdx < rdi)     subq %rdi, %rdx
                          //     goto body        jl body
                          //   goto end           jmp end
                          // body:              body:
    rax = rax + rdi;      //   rax = rax + rdi    addq %rdi, %rax    
    rsi = rsi + 1;        //   rsi = rsi + rcx    addq %rcx, %rsi
                          //   goto loop          jmp loop
  }                       // end:               end:
  return rax;             //   return rax         ret
}

