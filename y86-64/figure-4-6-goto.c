#include <stdio.h>

long rax;
long *rdi;
long rsi;
long r12;

void sum_array() {
    // save r12
    rax = 0;
loop:
    if (rsi == 0)
        goto end;
    r12 = *rdi;
    rax = rax + r12;
    r12 = 1; // Scale to 8 for assembly
    rdi = rdi + r12;
    r12 = 1;
    rsi = rsi - r12;
    goto loop;
end:
    // restore r12
}

int main(int argc, char *argv[]) {
    long array[] = {1, 2, 3, 4};
    long len = 4;
    rdi = array;
    rsi = len;
    sum_array();
    printf("rax = %ld\n", rax);
    return 0;
} 