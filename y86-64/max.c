#include <limits.h>
#include <stdio.h>

long testArray[] = {101, 4, 10, 100, 6, 5, 102};

long maxElement(long *array, long size) {
    long max = LONG_MIN;

    if (size == 1) {
        return *array;
    }

    max = maxElement(array+1, --size);
    if (*array > max) {
        max = *array;
    }
    
    return max;
}

int main() {
    long max = maxElement(testArray, 7);
    printf("max = %ld\n", max);
    return 0;
}
