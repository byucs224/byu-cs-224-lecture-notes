#include <stdio.h>

void insert_key_in_sorted_order(long *begin, long* j, long key) {
    long tmp = 0;
loop_0: 
    if (j < begin)
        goto end_0;
    if (*j <= key)
        goto end_0;
        tmp = *j;
        *(j + 1) = tmp;
        j = j - 1;
    goto loop_0;
end_0:
    *(j + 1) = key;
}

void insertion_sort(long *array, long *end) {
    long *i = array + 1;
    long *tmp = NULL;
    long key = 0;
loop_1:
    if (i >= end)
        goto end_1;
    key = *i;
    tmp = i - 1;
    insert_key_in_sorted_order(array, tmp, key);
    i = i + 1;
    goto loop_1;
end_1:
}

int main() {
    long array[] = {12, 11, 13, 5, 6};
    long length = sizeof(array) / sizeof(array[0]);

    insertion_sort(array, array + 5);

    printf("Sorted array: \n");
    for (long i = 0; i < length; i++) {
        printf("%ld ", array[i]);
    }
    printf("\n");

    return 0;
}