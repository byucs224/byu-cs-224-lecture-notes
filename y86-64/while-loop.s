sum_while:
	movl	$0, %edx
	movl	$1, %eax
	jmp	.L2
.L3:
	addq	%rdi, %rax
	addq	$1, %rdx
.L2:
	cmpq	%rdi, %rdx
	jl	.L3
	rep ret
