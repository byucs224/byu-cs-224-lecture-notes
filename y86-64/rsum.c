#include <stdio.h>

long rsum(long n) {
    long result;
    if (n == 1)
        result = 1;
    else
        result = n + rsum(n - 1);
    return result;
}

int main() { 
    long sum = rsum(5);
    printf("sum = %ld\n", sum);
    return 0;
}