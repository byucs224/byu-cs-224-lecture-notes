#include <stdio.h>

long recursive_sum(long *array, long length) {
  long sum = 0;
  if (length == 0) {
    return sum;
  }
  sum = recursive_sum(array + 1, length - 1);
  sum += *array;
  return sum;  
}

int main() {
  long array[] = {1,2,3,4};
  long length = 4;
  long sum = recursive_sum(array, length);
  printf("sum = %ld\n", sum);
  return 0;
}