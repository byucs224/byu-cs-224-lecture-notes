#include <stdio.h>

void insert_key_in_sorted_order(long *array, long j, long key) {
    while (j >= 0 && array[j] > key) {
        array[j + 1] = array[j];
        j = j - 1;
    }
    array[j + 1] = key;
}

void insertion_sort(long *array, long length) {
    long i = 1;
    while (i < length) {
        long key = array[i];
        long j = i - 1;
        insert_key_in_sorted_order(array, j, key);
        ++i;
    }
}

int main() {
    long array[] = {12, 11, 13, 5, 6};
    long length = sizeof(array) / sizeof(array[0]);

    insertion_sort(array, length);

    printf("Sorted array: \n");
    for (long i = 0; i < length; i++) {
        printf("%ld ", array[i]);
    }
    printf("\n");

    return 0;
}