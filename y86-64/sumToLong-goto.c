long sumToLong(long rdi);

void main() {
  long rdi = 5;             // irmovq $5, %rdi
  long rax = sumToLong(rdi);// call sumToLong
  return;                   // ret
}

long sumToLong(long rdi) {
  long rax = 0;             //   xorq %rax, %rax
  long rsi = 1;             //   irmovq $1, %rsi
loop:                       // loop:
  rax = rax + rdi;          //   addq %rdi, %rax
  rdi = rdi - rsi;          //   subq %rsi, %rdi
  if rdi > rsi {            //   rrmovq %rdi, %rdx
                            //   subq %rsi, %rdx    
      goto loop;            //   jg loop
  }                         //
end:                        // end:
  return rax;               //   ret
}

