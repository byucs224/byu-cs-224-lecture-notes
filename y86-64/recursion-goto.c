#include <stdio.h>

void *stack[100];
void **rsp = stack + 100;
long rax;
long *rdi;
long rsi;

void push(void *i) {
    rsp -= 1;
    *rsp = i;
}

void* pop() {
    void *i = *rsp;
    rsp += 1;
    return i;
}

void recursive_sum() {
    rax = 0;
    if (rsi == 0) {
      return;
    }
    push((void*)rsi);
    push((void*)rdi);
    long r10 = 1; // scale to 8
    rdi = rdi + r10;
    r10 = 1;
    rsi = rsi - r10;
    recursive_sum();
    rdi = (long*)(pop());
    rsi = (long)(pop());  
    r10 = *rdi;
    rax = rax + r10;
    return;  
}

int main() {
  long array[] = {1,2,3,4};
  long length = 4;
  rdi = array;
  rsi = length;
  recursive_sum();
  printf("sum = %ld\n", rax);
  return 0;
}