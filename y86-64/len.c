long len(long a[]) {
    long len;
    for (len = 0; a[len]; len++);
    return len;
}
