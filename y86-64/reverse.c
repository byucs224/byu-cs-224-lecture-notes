#include <stdio.h>

void swap(long *a, long *b) {
    long tmp = *a;
    *a = *b;
    *b = tmp;
}

void reverse(long *start, long *end) {
    if (start >= end) {
        return;
    }
    swap(start, end);
    reverse(start + 1, end - 1);
}

int main() {
    long seq[] = {1, 2, 3, 4, 5};
    reverse(seq, seq + 4);
    for (long i = 0 ; i < 5 ; ++i) {
        printf("%ld\t", seq[i]);
    }
    printf("\n");
    return 0;
}