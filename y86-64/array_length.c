#include <stdio.h>

// To compile and compare
// gcc -S -O1 -fcf-protection=none -fno-stack-protector array_length.c -o array_length.s

long array_length(long *array) {
    if (*array == 0) {
        return 1;
    } else {
        return 1 + array_length(array + 1);
    }
}

long main() {
    long array[] = {1, 2, 3, 4, 5, 0};
    long ans = array_length(array);
    printf("ans = %ld\n", ans);
    return 0;
}