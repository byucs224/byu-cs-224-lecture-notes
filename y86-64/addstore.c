long add2(long a, long b) {
    long s = a + b;
    return s;
}

void add_store(long x, long y, long *dest) {
    long t = mult2(x, y);
    *dest = t;
}

void main(void) {
    long x = 0;
    add_store(10, 20, &x);
}
