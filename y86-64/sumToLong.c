long sumToLong(long rdi);

void main() {
  long rdi = 5;
  long rax = sumToLong(rdi);
  return;
}

long sumToLong(long rdi) {   
  long rax = 0;              
                                 
  do {                        
    rax = rax + rdi;          
    rdi = rdi - 1;
  } while (rdi > 1);
  
  return rax;
}

