long square(long rdi);

void main() {
  long rdi = 5;
  long rax = square(rdi);  // call square
  return;
}

long square(long rdi) {   // square:
  long rax = rdi;         //   rrmovq %rdi, %rax   rax = rdi                 
  long rsi = 1;           //   irmovq $1, %rsi     rsi = 1
                          //   rrmovq %rsi, %rdx   rdx = 1
  while (rsi < rdi) {     // loop:
                          //   rrmovq %rsi, %rcx   rcx = rsi
                          //   subq %rdi, %rcx     if (rcx < rsi) // rcx = rcx - rdi
                          //   jl body               goto body
                          //   jmp end             goto end
                          // body:
    rax = rax + rdi;      //   addq %rdi, %rax     rax = rax + rdi
    rsi = rsi + 1;        //   addq %rdx, %rsi     rsi = rsi + rdx
                          //   jmp loop            goto loop
  }                       // end:
  return rax;             //   ret                 return rax
}

