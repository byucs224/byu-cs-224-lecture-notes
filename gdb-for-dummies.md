# GDB Primer

GDB stands for *GNU Debugger*.
It can be used to view how a program's instructions affect the memory and registers while the program is being run.
It can also be used to pause the program and execute instructions one-at-a-time, letting you see their effect in great detail.

To run a programmer under the GNU Debugger, use the command **gdb -tui a.out** (replacing **a.out** with the name of the executable you want to debug).
The **-tui** flag tells **gdb** to run with the *text user interface*, which is an attempt to offer the convenience of a *graphical user interface* in a terminal.

Running the command **gdb -tui a.out** alone will not cause your program to execute.
Instead, we are presented with a prompt.
Not executing the program automatically is a good thing, as it allows us to set *breakpoints* before the program starts executing.
A *breakpoint* is a place in the code at which **gdb** should pause the program.
With program execution paused, we can inspect memory and registers and see what instructions will be executed next.

Sometimes the `-tui` interface (and any of the `layout` interfaces below) get garbled up and do not update correctly. The **refresh** command will update the layouts and fix the issue.

We can use the command **break main** to set a breakpoint on *main*.
Doing so tells *gdb* to pause execution when the *main* function is reached. Use **break \*main+offset** to break at a specific instruction in the *main* method. If you want to break in the first instruction in main, then use `break *main`. The `*main` breaks on the first instruction whereas `main` alone breaks at the first instruction after the prolog for `main`.

Although we have set a breakpoint, the execution of our program has not begun.
We accomplish that with the **run** command.

Once you execute the **run** command, you should see a line that begins *Breakpoint 1...* with a prompt underneath it.
This is a signal that the program is paused at breakpoint 1.
From this point, we can view the instructions to be executed with the command **layout asm**.
The command **layout split** will show the source code corresponding to the assembly code, if it's available.
We can also view the registers with **layout regs**. Use `ctrl-x o` to change focus between the windows.

To step one instruction, we use the command **si**.
If the current instruction is a *callq* instruction, initiating a call, then the **si** command will proceed to the first instruction of the called procedure.
If, on the other hand, we want to execute the program until the call returns, we use the *next instruction* command **ni**.
This command will execute the program until it reaches the next instruction in the current procedure.
If the current instruction is a call, the next instruction is reached when that call returns.

To print the value of the *rax* register, use the command **p $rax** (note the *$*).
To interpret the contents as a *float*, use the command **p/f $rax**.
We can replace the **f** in **p/f** with any *printf* format specifier (such as *d*, *c*, *x*) to interpret the contents in other ways.

If register *rax* contains an address, the **x $rax** command will print the contents of memory at that address.
The register **rax** can be replaced with any other register.
Also, **x/f** is to **p/f** as **x** is to **p**.

The command **frame** displays information about the currently-selected stack frame (which initially is the immediate stack frame).
The command **backtrace** displays information about all frames on the stack.

## For more information

You can find the GDB manual [here](https://ftp.gnu.org/old-gnu/Manuals/gdb/html_node/gdb_toc.html) and information about the TUI specifically [here](https://ftp.gnu.org/old-gnu/Manuals/gdb/html_chapter/gdb_19.html).

Figure 3.39 of the textbook (pg 280) contains some useful GDB commands.

[This web page](http://condor.depaul.edu/glancast/373class/docs/gdb.html) contains a great, gentle walkthrough of GDB.

