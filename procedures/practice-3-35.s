rfun:
	testq	%rdi, %rdi
	je	.L3
	pushq	%rbx
	movq	%rdi, %rbx
	sarq	$2, %rdi
	call	rfun
	addq	%rbx, %rax
	jmp	.L2
.L3:
	movl	$0, %eax
	ret
.L2:
	popq	%rbx
	ret
