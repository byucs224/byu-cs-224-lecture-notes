struct small {
    char name[16];
    int x;
    int y;
};

int plus(struct small t) {
    return (t.x + t.y);
}


int main(void) {
    struct small t;
    t.x = 10;
    t.y = 11;
    return plus(t);
}

