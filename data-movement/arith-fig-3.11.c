#include <stdio.h>

long arith(long x, long y, long z) {
    long t1 = x ^ y;
    long t2 = z * 48;
    long t3 = t1 & 0x0f0f0f0f;
    long t4 = t2 - t3;
    return t4;
}

int main(int argc, char *argv[]) {
    printf("Data movement is easy\n");
    printf("arith(%ld, %ld, %ld) = %ld\n", 10l, 20l, 30l, arith(10,20,30));
    return 0;
}
