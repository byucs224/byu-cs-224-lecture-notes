#include <stdio.h>

void swap(long *xp, long *yp) {
    long t0 = *xp;
    long t1 = *yp;
    *xp = t1;
    *yp = t0;
}

int main(int argc, char* argv[]) {
  long x = 10;
  long y = 20;
  printf("x = %ld\ty = %ld\n", x, y);
  swap(&x, &y);
  printf("x = %ld\ty = %ld\n", x, y);
}
