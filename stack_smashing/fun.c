#include <stdio.h>

typedef struct {
    int a[2];
    long d;
} struct_t;

long fun(int i) {
    volatile struct_t s;
    s.d = 0xdeadbeef;
    s.a[i] = 0x87654321;
    return s.d;
}

int main(int argc, char *argv[]) {
    printf("fun(%d) = %lx\n", argc - 1, fun(argc - 1));
    return 0;
}
