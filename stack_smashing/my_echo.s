	.file	"my_echo.c"
	.text
.Ltext0:
	.globl	echo
	.type	echo, @function
echo:
.LFB41:
	.file 1 "my_echo.c"
	.loc 1 4 0
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	subq	$16, %rsp
	.cfi_def_cfa_offset 32
	.loc 1 6 0
	leaq	12(%rsp), %rbx
	movq	%rbx, %rdi
	movl	$0, %eax
	call	gets@PLT
.LVL0:
	.loc 1 7 0
	movq	%rbx, %rdi
	call	puts@PLT
.LVL1:
	.loc 1 8 0
	addq	$16, %rsp
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE41:
	.size	echo, .-echo
	.globl	call_echo
	.type	call_echo, @function
call_echo:
.LFB42:
	.loc 1 10 0
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	.loc 1 11 0
	movl	$0, %eax
	call	echo
.LVL2:
	.loc 1 12 0
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE42:
	.size	call_echo, .-call_echo
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"attack 0!"
	.text
	.globl	attack0
	.type	attack0, @function
attack0:
.LFB43:
	.loc 1 14 0
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
.LVL3:
.LBB12:
.LBB13:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/stdio2.h"
	.loc 2 104 0
	leaq	.LC0(%rip), %rdi
	call	puts@PLT
.LVL4:
.LBE13:
.LBE12:
	.loc 1 16 0
	movl	$1, %edi
	call	exit@PLT
.LVL5:
	.cfi_endproc
.LFE43:
	.size	attack0, .-attack0
	.section	.rodata.str1.1
.LC1:
	.string	"attack 1!"
	.text
	.globl	attack1
	.type	attack1, @function
attack1:
.LFB44:
	.loc 1 19 0
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
.LVL6:
.LBB14:
.LBB15:
	.loc 2 104 0
	leaq	.LC1(%rip), %rdi
	call	puts@PLT
.LVL7:
.LBE15:
.LBE14:
	.loc 1 21 0
	movl	$1, %edi
	call	exit@PLT
.LVL8:
	.cfi_endproc
.LFE44:
	.size	attack1, .-attack1
	.section	.rodata.str1.1
.LC2:
	.string	"attack 2!"
	.text
	.globl	attack2
	.type	attack2, @function
attack2:
.LFB45:
	.loc 1 24 0
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
.LVL9:
.LBB16:
.LBB17:
	.loc 2 104 0
	leaq	.LC2(%rip), %rdi
	call	puts@PLT
.LVL10:
.LBE17:
.LBE16:
	.loc 1 26 0
	movl	$1, %edi
	call	exit@PLT
.LVL11:
	.cfi_endproc
.LFE45:
	.size	attack2, .-attack2
	.section	.rodata.str1.1
.LC3:
	.string	"attack 3!"
	.text
	.globl	attack3
	.type	attack3, @function
attack3:
.LFB46:
	.loc 1 29 0
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
.LVL12:
.LBB18:
.LBB19:
	.loc 2 104 0
	leaq	.LC3(%rip), %rdi
	call	puts@PLT
.LVL13:
.LBE19:
.LBE18:
	.loc 1 31 0
	movl	$1, %edi
	call	exit@PLT
.LVL14:
	.cfi_endproc
.LFE46:
	.size	attack3, .-attack3
	.section	.rodata.str1.1
.LC4:
	.string	"attack 4!"
	.text
	.globl	attack4
	.type	attack4, @function
attack4:
.LFB47:
	.loc 1 34 0
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
.LVL15:
.LBB20:
.LBB21:
	.loc 2 104 0
	leaq	.LC4(%rip), %rdi
	call	puts@PLT
.LVL16:
.LBE21:
.LBE20:
	.loc 1 36 0
	movl	$1, %edi
	call	exit@PLT
.LVL17:
	.cfi_endproc
.LFE47:
	.size	attack4, .-attack4
	.globl	main
	.type	main, @function
main:
.LFB48:
	.loc 1 39 0
	.cfi_startproc
.LVL18:
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	.loc 1 40 0
	movl	$0, %eax
	call	call_echo
.LVL19:
	.loc 1 43 0
	movl	$0, %eax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE48:
	.size	main, .-main
.Letext0:
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/libio.h"
	.file 6 "/usr/include/stdio.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 8 "/usr/include/stdlib.h"
	.file 9 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x6d5
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF68
	.byte	0xc
	.long	.LASF69
	.long	.LASF70
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF7
	.byte	0x3
	.byte	0xd8
	.long	0x38
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.long	.LASF2
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.long	.LASF3
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x2
	.long	.LASF8
	.byte	0x4
	.byte	0x8c
	.long	0x69
	.uleb128 0x2
	.long	.LASF9
	.byte	0x4
	.byte	0x8d
	.long	0x69
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0x8e
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF10
	.uleb128 0x7
	.long	0x8e
	.uleb128 0x8
	.long	.LASF40
	.byte	0xd8
	.byte	0x5
	.byte	0xf5
	.long	0x21a
	.uleb128 0x9
	.long	.LASF11
	.byte	0x5
	.byte	0xf6
	.long	0x62
	.byte	0
	.uleb128 0x9
	.long	.LASF12
	.byte	0x5
	.byte	0xfb
	.long	0x88
	.byte	0x8
	.uleb128 0x9
	.long	.LASF13
	.byte	0x5
	.byte	0xfc
	.long	0x88
	.byte	0x10
	.uleb128 0x9
	.long	.LASF14
	.byte	0x5
	.byte	0xfd
	.long	0x88
	.byte	0x18
	.uleb128 0x9
	.long	.LASF15
	.byte	0x5
	.byte	0xfe
	.long	0x88
	.byte	0x20
	.uleb128 0x9
	.long	.LASF16
	.byte	0x5
	.byte	0xff
	.long	0x88
	.byte	0x28
	.uleb128 0xa
	.long	.LASF17
	.byte	0x5
	.value	0x100
	.long	0x88
	.byte	0x30
	.uleb128 0xa
	.long	.LASF18
	.byte	0x5
	.value	0x101
	.long	0x88
	.byte	0x38
	.uleb128 0xa
	.long	.LASF19
	.byte	0x5
	.value	0x102
	.long	0x88
	.byte	0x40
	.uleb128 0xa
	.long	.LASF20
	.byte	0x5
	.value	0x104
	.long	0x88
	.byte	0x48
	.uleb128 0xa
	.long	.LASF21
	.byte	0x5
	.value	0x105
	.long	0x88
	.byte	0x50
	.uleb128 0xa
	.long	.LASF22
	.byte	0x5
	.value	0x106
	.long	0x88
	.byte	0x58
	.uleb128 0xa
	.long	.LASF23
	.byte	0x5
	.value	0x108
	.long	0x262
	.byte	0x60
	.uleb128 0xa
	.long	.LASF24
	.byte	0x5
	.value	0x10a
	.long	0x268
	.byte	0x68
	.uleb128 0xa
	.long	.LASF25
	.byte	0x5
	.value	0x10c
	.long	0x62
	.byte	0x70
	.uleb128 0xa
	.long	.LASF26
	.byte	0x5
	.value	0x110
	.long	0x62
	.byte	0x74
	.uleb128 0xa
	.long	.LASF27
	.byte	0x5
	.value	0x112
	.long	0x70
	.byte	0x78
	.uleb128 0xa
	.long	.LASF28
	.byte	0x5
	.value	0x116
	.long	0x46
	.byte	0x80
	.uleb128 0xa
	.long	.LASF29
	.byte	0x5
	.value	0x117
	.long	0x54
	.byte	0x82
	.uleb128 0xa
	.long	.LASF30
	.byte	0x5
	.value	0x118
	.long	0x26e
	.byte	0x83
	.uleb128 0xa
	.long	.LASF31
	.byte	0x5
	.value	0x11c
	.long	0x27e
	.byte	0x88
	.uleb128 0xa
	.long	.LASF32
	.byte	0x5
	.value	0x125
	.long	0x7b
	.byte	0x90
	.uleb128 0xa
	.long	.LASF33
	.byte	0x5
	.value	0x12d
	.long	0x86
	.byte	0x98
	.uleb128 0xa
	.long	.LASF34
	.byte	0x5
	.value	0x12e
	.long	0x86
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF35
	.byte	0x5
	.value	0x12f
	.long	0x86
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF36
	.byte	0x5
	.value	0x130
	.long	0x86
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF37
	.byte	0x5
	.value	0x132
	.long	0x2d
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF38
	.byte	0x5
	.value	0x133
	.long	0x62
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF39
	.byte	0x5
	.value	0x135
	.long	0x284
	.byte	0xc4
	.byte	0
	.uleb128 0xb
	.long	0x8e
	.long	0x22a
	.uleb128 0xc
	.long	0x38
	.byte	0x3
	.byte	0
	.uleb128 0xd
	.long	.LASF71
	.byte	0x5
	.byte	0x9a
	.uleb128 0x8
	.long	.LASF41
	.byte	0x18
	.byte	0x5
	.byte	0xa0
	.long	0x262
	.uleb128 0x9
	.long	.LASF42
	.byte	0x5
	.byte	0xa1
	.long	0x262
	.byte	0
	.uleb128 0x9
	.long	.LASF43
	.byte	0x5
	.byte	0xa2
	.long	0x268
	.byte	0x8
	.uleb128 0x9
	.long	.LASF44
	.byte	0x5
	.byte	0xa6
	.long	0x62
	.byte	0x10
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x231
	.uleb128 0x6
	.byte	0x8
	.long	0x9a
	.uleb128 0xb
	.long	0x8e
	.long	0x27e
	.uleb128 0xc
	.long	0x38
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x22a
	.uleb128 0xb
	.long	0x8e
	.long	0x294
	.uleb128 0xc
	.long	0x38
	.byte	0x13
	.byte	0
	.uleb128 0xe
	.long	.LASF72
	.uleb128 0xf
	.long	.LASF45
	.byte	0x5
	.value	0x13f
	.long	0x294
	.uleb128 0xf
	.long	.LASF46
	.byte	0x5
	.value	0x140
	.long	0x294
	.uleb128 0xf
	.long	.LASF47
	.byte	0x5
	.value	0x141
	.long	0x294
	.uleb128 0x6
	.byte	0x8
	.long	0x95
	.uleb128 0x7
	.long	0x2bd
	.uleb128 0x10
	.long	0x2bd
	.uleb128 0x11
	.long	.LASF48
	.byte	0x6
	.byte	0x87
	.long	0x268
	.uleb128 0x11
	.long	.LASF49
	.byte	0x6
	.byte	0x88
	.long	0x268
	.uleb128 0x11
	.long	.LASF50
	.byte	0x6
	.byte	0x89
	.long	0x268
	.uleb128 0x11
	.long	.LASF51
	.byte	0x7
	.byte	0x1a
	.long	0x62
	.uleb128 0xb
	.long	0x2c3
	.long	0x304
	.uleb128 0x12
	.byte	0
	.uleb128 0x7
	.long	0x2f9
	.uleb128 0x11
	.long	.LASF52
	.byte	0x7
	.byte	0x1b
	.long	0x304
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF53
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF54
	.uleb128 0x13
	.long	.LASF73
	.byte	0x1
	.byte	0x27
	.long	0x62
	.quad	.LFB48
	.quad	.LFE48-.LFB48
	.uleb128 0x1
	.byte	0x9c
	.long	0x36f
	.uleb128 0x14
	.long	.LASF55
	.byte	0x1
	.byte	0x27
	.long	0x62
	.long	.LLST5
	.uleb128 0x14
	.long	.LASF56
	.byte	0x1
	.byte	0x27
	.long	0x36f
	.long	.LLST6
	.uleb128 0x15
	.quad	.LVL19
	.long	0x5aa
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x88
	.uleb128 0x16
	.long	.LASF57
	.byte	0x1
	.byte	0x22
	.quad	.LFB47
	.quad	.LFE47-.LFB47
	.uleb128 0x1
	.byte	0x9c
	.long	0x3e6
	.uleb128 0x17
	.long	0x63e
	.quad	.LBB20
	.quad	.LBE20-.LBB20
	.byte	0x1
	.byte	0x23
	.long	0x3d2
	.uleb128 0x18
	.long	0x64e
	.long	.LLST4
	.uleb128 0x19
	.quad	.LVL16
	.long	0x66a
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC4
	.byte	0
	.byte	0
	.uleb128 0x19
	.quad	.LVL17
	.long	0x679
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x16
	.long	.LASF58
	.byte	0x1
	.byte	0x1d
	.quad	.LFB46
	.quad	.LFE46-.LFB46
	.uleb128 0x1
	.byte	0x9c
	.long	0x457
	.uleb128 0x17
	.long	0x63e
	.quad	.LBB18
	.quad	.LBE18-.LBB18
	.byte	0x1
	.byte	0x1e
	.long	0x443
	.uleb128 0x18
	.long	0x64e
	.long	.LLST3
	.uleb128 0x19
	.quad	.LVL13
	.long	0x66a
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC3
	.byte	0
	.byte	0
	.uleb128 0x19
	.quad	.LVL14
	.long	0x679
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x16
	.long	.LASF59
	.byte	0x1
	.byte	0x18
	.quad	.LFB45
	.quad	.LFE45-.LFB45
	.uleb128 0x1
	.byte	0x9c
	.long	0x4c8
	.uleb128 0x17
	.long	0x63e
	.quad	.LBB16
	.quad	.LBE16-.LBB16
	.byte	0x1
	.byte	0x19
	.long	0x4b4
	.uleb128 0x18
	.long	0x64e
	.long	.LLST2
	.uleb128 0x19
	.quad	.LVL10
	.long	0x66a
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC2
	.byte	0
	.byte	0
	.uleb128 0x19
	.quad	.LVL11
	.long	0x679
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x16
	.long	.LASF60
	.byte	0x1
	.byte	0x13
	.quad	.LFB44
	.quad	.LFE44-.LFB44
	.uleb128 0x1
	.byte	0x9c
	.long	0x539
	.uleb128 0x17
	.long	0x63e
	.quad	.LBB14
	.quad	.LBE14-.LBB14
	.byte	0x1
	.byte	0x14
	.long	0x525
	.uleb128 0x18
	.long	0x64e
	.long	.LLST1
	.uleb128 0x19
	.quad	.LVL7
	.long	0x66a
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.byte	0
	.byte	0
	.uleb128 0x19
	.quad	.LVL8
	.long	0x679
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x16
	.long	.LASF61
	.byte	0x1
	.byte	0xe
	.quad	.LFB43
	.quad	.LFE43-.LFB43
	.uleb128 0x1
	.byte	0x9c
	.long	0x5aa
	.uleb128 0x17
	.long	0x63e
	.quad	.LBB12
	.quad	.LBE12-.LBB12
	.byte	0x1
	.byte	0xf
	.long	0x596
	.uleb128 0x18
	.long	0x64e
	.long	.LLST0
	.uleb128 0x19
	.quad	.LVL4
	.long	0x66a
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.byte	0
	.byte	0
	.uleb128 0x19
	.quad	.LVL5
	.long	0x679
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x16
	.long	.LASF62
	.byte	0x1
	.byte	0xa
	.quad	.LFB42
	.quad	.LFE42-.LFB42
	.uleb128 0x1
	.byte	0x9c
	.long	0x5d5
	.uleb128 0x15
	.quad	.LVL2
	.long	0x5d5
	.byte	0
	.uleb128 0x16
	.long	.LASF63
	.byte	0x1
	.byte	0x4
	.quad	.LFB41
	.quad	.LFE41-.LFB41
	.uleb128 0x1
	.byte	0x9c
	.long	0x63e
	.uleb128 0x1b
	.long	.LASF74
	.byte	0x1
	.byte	0x5
	.long	0x21a
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x1c
	.long	.LASF66
	.byte	0x1
	.byte	0x6
	.long	0x62
	.long	0x611
	.uleb128 0x1d
	.byte	0
	.uleb128 0x1e
	.quad	.LVL0
	.long	0x6c1
	.long	0x629
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x19
	.quad	.LVL1
	.long	0x6cc
	.uleb128 0x1a
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.long	.LASF64
	.byte	0x2
	.byte	0x66
	.long	0x62
	.byte	0x3
	.long	0x65b
	.uleb128 0x20
	.long	.LASF75
	.byte	0x2
	.byte	0x66
	.long	0x2c8
	.uleb128 0x1d
	.byte	0
	.uleb128 0x21
	.uleb128 0xd
	.byte	0x9e
	.uleb128 0xb
	.byte	0x61
	.byte	0x74
	.byte	0x74
	.byte	0x61
	.byte	0x63
	.byte	0x6b
	.byte	0x20
	.byte	0x34
	.byte	0x21
	.byte	0xa
	.byte	0
	.uleb128 0x22
	.long	.LASF67
	.long	.LASF76
	.byte	0x9
	.byte	0
	.long	.LASF67
	.uleb128 0x23
	.long	.LASF65
	.long	.LASF65
	.byte	0x8
	.value	0x266
	.uleb128 0x21
	.uleb128 0xd
	.byte	0x9e
	.uleb128 0xb
	.byte	0x61
	.byte	0x74
	.byte	0x74
	.byte	0x61
	.byte	0x63
	.byte	0x6b
	.byte	0x20
	.byte	0x33
	.byte	0x21
	.byte	0xa
	.byte	0
	.uleb128 0x21
	.uleb128 0xd
	.byte	0x9e
	.uleb128 0xb
	.byte	0x61
	.byte	0x74
	.byte	0x74
	.byte	0x61
	.byte	0x63
	.byte	0x6b
	.byte	0x20
	.byte	0x32
	.byte	0x21
	.byte	0xa
	.byte	0
	.uleb128 0x21
	.uleb128 0xd
	.byte	0x9e
	.uleb128 0xb
	.byte	0x61
	.byte	0x74
	.byte	0x74
	.byte	0x61
	.byte	0x63
	.byte	0x6b
	.byte	0x20
	.byte	0x31
	.byte	0x21
	.byte	0xa
	.byte	0
	.uleb128 0x21
	.uleb128 0xd
	.byte	0x9e
	.uleb128 0xb
	.byte	0x61
	.byte	0x74
	.byte	0x74
	.byte	0x61
	.byte	0x63
	.byte	0x6b
	.byte	0x20
	.byte	0x30
	.byte	0x21
	.byte	0xa
	.byte	0
	.uleb128 0x24
	.long	.LASF66
	.long	.LASF66
	.byte	0x1
	.byte	0x6
	.uleb128 0x23
	.long	.LASF67
	.long	.LASF67
	.byte	0x6
	.value	0x278
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x36
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST5:
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL19-1-.Ltext0
	.quad	.LFE48-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LLST6:
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL19-1-.Ltext0
	.quad	.LFE48-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LLST4:
	.quad	.LVL15-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+1627
	.sleb128 0
	.quad	0
	.quad	0
.LLST3:
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+1669
	.sleb128 0
	.quad	0
	.quad	0
.LLST2:
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+1684
	.sleb128 0
	.quad	0
	.quad	0
.LLST1:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+1699
	.sleb128 0
	.quad	0
	.quad	0
.LLST0:
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+1714
	.sleb128 0
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF64:
	.string	"printf"
.LASF8:
	.string	"__off_t"
.LASF12:
	.string	"_IO_read_ptr"
.LASF24:
	.string	"_chain"
.LASF7:
	.string	"size_t"
.LASF30:
	.string	"_shortbuf"
.LASF47:
	.string	"_IO_2_1_stderr_"
.LASF18:
	.string	"_IO_buf_base"
.LASF61:
	.string	"attack0"
.LASF60:
	.string	"attack1"
.LASF54:
	.string	"long long unsigned int"
.LASF58:
	.string	"attack3"
.LASF57:
	.string	"attack4"
.LASF53:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF25:
	.string	"_fileno"
.LASF13:
	.string	"_IO_read_end"
.LASF6:
	.string	"long int"
.LASF11:
	.string	"_flags"
.LASF19:
	.string	"_IO_buf_end"
.LASF28:
	.string	"_cur_column"
.LASF27:
	.string	"_old_offset"
.LASF32:
	.string	"_offset"
.LASF41:
	.string	"_IO_marker"
.LASF48:
	.string	"stdin"
.LASF3:
	.string	"unsigned int"
.LASF0:
	.string	"long unsigned int"
.LASF72:
	.string	"_IO_FILE_plus"
.LASF63:
	.string	"echo"
.LASF16:
	.string	"_IO_write_ptr"
.LASF66:
	.string	"gets"
.LASF51:
	.string	"sys_nerr"
.LASF43:
	.string	"_sbuf"
.LASF2:
	.string	"short unsigned int"
.LASF20:
	.string	"_IO_save_base"
.LASF31:
	.string	"_lock"
.LASF26:
	.string	"_flags2"
.LASF38:
	.string	"_mode"
.LASF76:
	.string	"__builtin_puts"
.LASF49:
	.string	"stdout"
.LASF45:
	.string	"_IO_2_1_stdin_"
.LASF67:
	.string	"puts"
.LASF17:
	.string	"_IO_write_end"
.LASF71:
	.string	"_IO_lock_t"
.LASF40:
	.string	"_IO_FILE"
.LASF44:
	.string	"_pos"
.LASF70:
	.string	"/users/faculty/egm/tmp/byu-cs-224-lecture-notes/stack_smashing"
.LASF23:
	.string	"_markers"
.LASF1:
	.string	"unsigned char"
.LASF5:
	.string	"short int"
.LASF29:
	.string	"_vtable_offset"
.LASF46:
	.string	"_IO_2_1_stdout_"
.LASF62:
	.string	"call_echo"
.LASF65:
	.string	"exit"
.LASF69:
	.string	"my_echo.c"
.LASF10:
	.string	"char"
.LASF74:
	.string	"buffer"
.LASF42:
	.string	"_next"
.LASF9:
	.string	"__off64_t"
.LASF14:
	.string	"_IO_read_base"
.LASF22:
	.string	"_IO_save_end"
.LASF75:
	.string	"__fmt"
.LASF33:
	.string	"__pad1"
.LASF34:
	.string	"__pad2"
.LASF35:
	.string	"__pad3"
.LASF36:
	.string	"__pad4"
.LASF37:
	.string	"__pad5"
.LASF39:
	.string	"_unused2"
.LASF50:
	.string	"stderr"
.LASF56:
	.string	"argv"
.LASF68:
	.string	"GNU C11 7.4.0 -mtune=generic -march=x86-64 -g -Og -fno-stack-protector"
.LASF21:
	.string	"_IO_backup_base"
.LASF52:
	.string	"sys_errlist"
.LASF55:
	.string	"argc"
.LASF59:
	.string	"attack2"
.LASF73:
	.string	"main"
.LASF15:
	.string	"_IO_write_base"
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
