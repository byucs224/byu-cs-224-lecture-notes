#include <stdio.h>
#include <stdlib.h>

void echo() {
  char buffer[4];
  gets(buffer);
  puts(buffer);
}

void call_echo() {
  echo();
}

void attack0() {
  printf( "attack 0!\n" );
  exit(1);
}

void attack1() {
  printf( "attack 1!\n" );
  exit(1);
}

void attack2() {
  printf( "attack 2!\n" );
  exit(1);
}

void attack3() {
  printf( "attack 3!\n" );
  exit(1);
}

void attack4() {
  printf( "attack 4!\n" );
  exit(1);
}

int main( int argc, char *argv[] ) {
  call_echo();

  return 0;
}
