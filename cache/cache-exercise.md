# Objective

Lookup an address in a cache and determine if it is a hit or a miss. If a hit, then return the data. If a miss, then evict data and load new data.

# Reading

   * Section 6.4 in textbook
   * [12-cache-memories.pptx](https://bitbucket.org/byucs224/byu-cs-224-lecture-notes/src/master/12-cache-memories.pptx)

# Outline

   * Cover to slide 17 in [12-cache-memories.pptx](https://bitbucket.org/byucs224/byu-cs-224-lecture-notes/src/master/12-cache-memories.pptx)
   * Do a class exercise:

   Show memory on the board:

   0000x: CS224 is a challenging class that teaches how a computer works. 

      * Addresses are 16-bits (2 bytes).
      * Block sizes are 8 bytes.

   Divide the class into three caches: 
   
      * Level-1: fully associative with two lines.
      * Level-2: 2-line direct mapped cache
      * Level-3: 4-line 2-way set associative

Have each group define the parts of each address and indicate those on the board.

Simulate the data movement in the caches for the following code assuming *a* is at address 0x0000 and everything else is stored in registers.

```c
#include <stdio.h>

int main() {
    char a[] = "CS224 is a challenging class that teaches how a computer works.";
    char *p = a;

    while (*p) {
        printf("%c", *p++);
    }
    printf("\n");
}
```

Change stride and access patterns to see how that affects the caches.