long dw_loop(long x) {
    long y = x * x;
    long *p = &x;
    long n = 2 * x;
    do {
        x += y;
        (*p)++;
        n--;
    } while (n > 0);
    return x;
}


/* Which registers are used to hold program values x, y, and n?

   How has the compiler eliminated the need for pointer variable p and the
   pointer dereferencing implied by the expression (*p)++?
*/
