# Outline

* Define row-major order for array layout in memory
* Read and understand array indexing from x86-64 code
* Find the dimensions of multi-dimensional arrays from x86-64 code accessing the arrays

# Reading

* Section 3.8 in the text
* Note that -Og or -O3 work for the compile

# Outline

* Review pointer scaling and the x86-64 notation Imm(r_b, r_d, s) for accessing array elements
* Do *practice-3-36.txt* and *practice-3-37.txt*
* Define row-major order and draw on the board in the memory diagram a multi-dimensional array (*first dimension is the row count and the second is the column count*)--draw Fig. 3.36 only reversed to match how we draw memory.
* Work slowly through *practice-3-38.s* and figure out the array dimensions from the assembly
* Repeat with different array dimensions

# Multi-dimensional Array Cliff-Notes

* The C-expression `int A[R][C]` creates an array of *R* rows with *C* columns in each row. The array is stored in a contiguous block of memory in **row-major** order---that means the array is stored one row at a time starting with the 0-th row, followed by the 1-th row, etc.
* In assembly, the address of the i-th row, so the C-expression `A[i]` is `A + i * C * sizeof(int)`. The *C* accounts for the number of columns in each row. The `sizeof(int)` accounts for the size of each element in the multi-dimensional array.
* The C-expression `int x = A[i][j]` becomes `x = M[A + (i*C + j) * sizeof(int)]` where *M* is memory.

## Breakdown of Practice 3.38

The problem is found in Section 3.8 on page 259 of the text.

```asm
# long P[M][N];
# long Q[N][M];
#
# long sum_elements(long i, long j) {
#     return P[i][j] + Q[j][i];
# }
# i: %rdi
# j: %rsi

sum_elements:
        leaq    0(,%rdi,8), %rdx       # %rdx = 8*i
        subq    %rdi, %rdx             # %rdx = 8*i - i = 7*i
        addq    %rsi, %rdx             # %rdx = 7*i + j
        leaq    0(%rsi,%rsi,4), %rax   # %rax = j + 4*j = 5*j
        addq    %rax, %rdi             # %rdi = 5*j + i
        movq    Q(,%rdi,8), %rax       # %rax = M[Q + (5*j + i)*8] --- 5 columns in each row of Q
        addq    P(,%rdx,8), %rax       # %rax = %rax + M[P + (7*i + j)*8] -- 7 columns in each row of P
        ret

# Figure out the values for M and N from the assembly code.
```

The comments explain each line. From the comments, since there are 5 columns in Q, then *M = 5*. And since there are 7 columns in P, then *N = 7*. Notice that the comments in last two lines of assemble code above exactly match the equation in the last bullet: `x = M[A + (i*C + j) * sizeof(int)]`.