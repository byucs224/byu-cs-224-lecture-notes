struct rec {
    long i;
    long j;
    long a[2];
    long *p;
};

void cp_stuff(struct rec *r) {
    r->j = r->i;
    r->p = &(r->a[r->j]);
}
