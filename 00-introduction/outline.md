# Objective

Understand tho different levels of abstraction to understanding a program.

# Outline

  * Write a simple C program (or C++) in an online IDE such as [onlinegdb.com](https://www.onlinegdb.com/online_c_compiler)
  * Write the same code in an editor, compile the code, and then run the code: `gcc <file>.c; ./a.out`
  * Compile the code into an object file, link the code, and then run the code: `gcc -c <file>.c; gcc -o <file> <file>.c`
  * Dissemble the code to show its dissembled form
  * Run `xxd -bits <file>` on the code to show its actual binary form
  * Show a schematic of the sequential architecture

  Abstraction is the key to computing. It manages the complexity

  This class is about breaking down the abstractions!

  Review Canvas.
  End with Growth Mindset: we all succeed together or we don't succeed at all.
  Trust in the Lord. Do your best. Let the Lord take care of the rest. Whatever the outcome, it will be consecrated to the welfare of your soul.


