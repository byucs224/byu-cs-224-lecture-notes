# Overview
Midterm 2 covers Chapter 4 through the Y86-64 sequential architecture in the textbook. It asks questions similar to `Lab 8: Y86-64 Assembly`, `Lab 10: Practice Midterm 2` and [Project 3: Y86-64](https://bitbucket.org/byucs224/byu-cs-224-projects/src/master/y86-64/y86-64.md). This is an open book exam, including the Y86-64 Quick Reference guide. You are also allowed one page of hand written notes. No other resources are allowed, be they friends, the internet, or y86-64 simulators. The target is 2 hours of student time to complete. 

# Comprehensive List of Topics

   * Determine the final state of the machine if it runs a given y86-64 program up to a specific program point where the y86-64 program is given in text form (e.g., play computer and report the final state of memory, registers, status, condition codes, and the `PC` for a given text form of a y86-64 program)
   * Match the given y86-64 code consisting of different move and `OPq` operations with an ending `ret` to one of three different C programs
   * Match the given y86-64 code consisting of conditional jumps, `Opq`, moves, a conditional move, and ending with a `ret` to one of three different C programs
   * Fill in the blanks on a partially given C program in a way that matches the computation in an additionally given complete y86-64 program (e.g., read the y86-64 code and use it to fill in the blanks for the C-program)
   * Assemble a given y86-64 program and report the contents of memory that represent that assembled program (e.g., populate memory in a way that it holds the indicated program)
   * Play computer by running a program starting at a specific point in memory and report the machine state after each instruction (e.g., decode and execute instructions from memory)
   * Create a computation table similar to the tables in the book used for [Project 3: Y86-64](https://bitbucket.org/byucs224/byu-cs-224-projects/src/master/y86-64/y86-64.md) to implement a new instructions for y86-64 (e.g., define what needs to happen in each stage of the sequential architecture for the new instruction to execute correctly)

# Suggested Practice Problems

The aforementioned labs. 