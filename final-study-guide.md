# Overview

The final exam is open book. One page of handwritten notes is also allowed. The target is 2 hours of student time to complete. 
The final focuses on topics covered since Midterm 2, specifically x86-64 and the topics relevant to the Bomb and Attack projects, as well as caches. 
By nature, some topics from earlier in the course (byte orderings, type sizes, memory layout, etc) will still be useful, but will not be the focus of questions. 

# Comprehensive List of Topics

   * Given a C program and its compiled x86-64 code, determine where local variables are allocated on the stack and what input is necessary to change the address to which a function will return. 
   * Given a C function and its compiled x86-64 code, determine the layout of that function's stack frame, including which variables are stored in registers and which on the stack (and why).
   * Given some sketchy C-code and the x86-64 code it compiled to, figure out input to corrupt the run-time stack in a way to change program behavior (Bomb Project, Attack Project).
   * Given different values for `m`, `C`, `B`, and `E` to describe different caches sizes and configurations, compute the number of sets `S`, the number of tag bits `t`, the number of set bits `s`, and the number of block offset bits `b` for each size and configuration (Lab 14).
   * Given a cache, with corresponding parameters and contents, determine the size of the cache, and whether accesses to given memory locations will result in hits or misses.  For hits, determine what would be returned for the memory access. 

# Topics saved for another exam, another day, for another class
   * Fill in a table showing the decimal or binary digits for a twos-complement representation using a given number of bits including the ability to identify minimum and maximum values and show what happens with expressions using minimum and maximum values ([Midterm 1](https://bitbucket.org/byucs224/byu-cs-224-lecture-notes/src/master/midterm-1-study-guide.md)).
   * Draw memory contents showing the data layout of local variables in a C program given starting addresses. And given a `scanf` command, indicate the needed input on `stdin` to elicit specific program output with `printf`. Be mindful of typecasting and its affect on the data input and output. Test topics in Lab 4 and the [class activity](https://bitbucket.org/byucs224/byu-cs-224-lecture-notes/src/master/C-Basics/pointers.md).
   * Determine the output from `printf` when widening (casting a `char` to something that uses more bytes) or narrowing (casting something with more bytes to a `char`) different types that start with constant values. Tests topics in [widening-narrowing.c](https://bitbucket.org/byucs224/byu-cs-224-lecture-notes/src/master/bit-level-encodings/widen-narrow.c) from class, Lab 4 and Lab 6
   * Fill in the blank lines in C-code according to compiled x86-64 code (Lab 11 and final two projects).
   * Given a cache size and configuration, an array of data structures, and various ways to traverse the array, determine the cache hit/miss rate for each version of the array traversal (Lab 14).


