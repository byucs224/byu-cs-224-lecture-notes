int f(int a, int b) {
loop:
  if (a >= b) {
    goto end;
  }
  a = a + 1;
  goto loop;

end:
  return a;
}

int main() {
  int a = 0;
  int b = 4;
  f(a,b);
  return 0;
}