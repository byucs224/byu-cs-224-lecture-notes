int f(int rdi, int rsi) {
  int r11 = 1;      // irmovq 0x1, %r11
loop:
  int r12 = rdi;    // rrmovq %rdi, %r12
  if (r12 >= rsi) { // subq %rsi, %r12
    goto end;       // jge end
  }
  rdi = rdi + r11;  // addq %r11, %rdi
  goto loop;        // jmp loop
end:
  rax = rdi         // rrmovq %rdi, %rax
  return rax        // ret
}

int main() {
  // int a = 0;
  int rdi = 0;
  int rsi = 4;
  f(a,b); // call f
  return 0; // halt
}

