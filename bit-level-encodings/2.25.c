#include <stdio.h>

float sum_elements(float a[], unsigned length) {
    int i;
    float result = 0;

    for (i = 0 ; i <= length - 1 ; ++i) {
        result += a[i];
    }

    return result;
}

int main(int argc, char* argv[]) {
    float a[] = {1.0, 2.0, 3.0};
    int length = 3;

    float result = sum_elements(a, length);
    printf("result = %f\n", result);
    
    return 0;
}
