#include <stdio.h>

/**
  1. Widen or narrow to now size according to rules for current type:

      * Narrowing is the same for all types --- truncate to size
      * Widen depends: sign-extend signed types and zero fill unsigned types

  2. Assign the new type.
*/

void show_bytes(unsigned char *ptr, size_t length) {
    int i = 0;

    while (i < length) {
        printf(" %.2x", ptr[i++]);
    }
    printf("\n");
}

int main(int argc, char *agrv[]) {
    int w = -1;
    unsigned int uw = 2147483648; // 2 to the 31st

    short sx = -12345;
    unsigned short usx = sx;

    int x = sx;
    unsigned int ux = usx;

    printf("w = %u = %d\n", w, w);
    printf("uw = %u = %d\n", uw, uw);

    printf("sx = %d: \t", sx);
    show_bytes((unsigned char *)&sx, sizeof(short));
    printf("usx = %u: \t", usx);
    show_bytes((unsigned char *)&usx, sizeof(unsigned short));
    printf("x = %d: \t", x);
    show_bytes((unsigned char *)&x, sizeof(int));
    printf("ux = %u: \t", ux);
    show_bytes((unsigned char *)&sx, sizeof(unsigned int));
    return 0;
}
