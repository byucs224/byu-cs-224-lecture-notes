#include <stdio.h>
#include <string.h>

int strlonger(char *s, char *t) {
    return ((strlen(s) - strlen(t)) > 0); 
}

int main(int argc, char* argv[]) {
    char lstr[] = "lllooonnnggg";
    char sstr[] = "shrt";

    printf("strlonger(ltr, sstr) = %d\n", strlonger(lstr, sstr));
    printf("strlonger(sstr, lstr) = %d\n", strlonger(sstr, lstr));
    
    return 0;
}
