#include <assert.h>

int main(int argc, char *agrv[]) {
    int x = who();  // Arbitrary value
    int y = knew(); // Arbitrary value

    unsigned ux = x;
    unsigned uy = y;

    // Which assertions could fail?

    assert ((x > 0) || (x - 1 < 0));
    assert ((x & 7) != 7 || (x << 29 < 0));
    assert ((x * x) >= 0);
    assert (x < 0 || -x <= 0);
    assert (x > 0 || -x >= 0);
    assert (x + y == uy + ux);
    // assert (x * ~y + uy * ux == -x);
}
