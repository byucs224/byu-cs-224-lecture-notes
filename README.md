These are the instructor lecture notes for BYU CS 224. These notes represent an
outline for the class experience and are not intended to be a complete
reference (e.g., they are more written for the instructor than the student).
That said, they provide a good list of topics covered in each class, example
problems, and references. The topics are those that matter most in the course,
and it is safe to use the notes as a study guide for exams.

As a fair warning, if the repository is cloned, then be sure to pull from time
to time as the notes are a *work-in-progress*. The below table is the order is
which the notes are used in the course with sub-lists as appropriates for
directories that require further ordering.

# Table of Contents

1.  [00-Introduction/outline.md](00-Introduction/outline.md)
2.  Command line (`ssh`, filesystem operations, compiling) ([command-line.md](command-line.md))
3.  I/O and basic types ([C-Basics/IO-types-statements.md](C-Basics/IO-types-statements.md))
4.  Arrays, strings, memory layout ([C-Basics/arrays.md](C-Basics/arrays.md)
5.  pointers.md (focus on *overflow.c* as it is key)

4.  bit-level-encodings

  1. 2.1.md (corresponding to section 2.1 in the text book
  2. 2.2.md 
  3. 2.3.md
  4. 2.4.md

5.  data-movement

  1. 3.4.md
  2. 3.5.md

