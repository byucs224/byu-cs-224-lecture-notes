# Overview

The shell is a non-gui interface to the file-system and
operating system. It gives a lot of control, automation, power, and debug
information for when things are not behaving. There are some folk who may
already know a little about the shell in this class. They are few. The
expectation is that this course is the first time students will see and use the
shell. It is targeted to the vast majority of the students in the course who
have never seen nor heard of the command-line. Don't be fooled by the few
students in class that seem to already know the material: they are the
few and not the majority (and even of those few, more likely than not, they know less than they realize).

## Teaching Aid

Use the Finder or Windows Explorer to connect the graphical view with the
command-line view.  Two ways to look at the same thing.

# Command line tutorials
  
  * [command-line bootcamp](http://rik.smith-unna.com/command_line_bootcamp/) 
  * [Ubuntu - Command Line](https://www.tutorialspoint.com/ubuntu/ubuntu_command_line.htm)
  * There are many tutorials at https://www.tutorialspoint.com/ which is used for the C/C++ stuff

# Online Shell

  * Show the [command-line bootcamp](http://rik.smith-unna.com/command_line_bootcamp/) online shell
  * Use the [CentOS](https://www.tutorialspoint.com/unix_terminal_online.php) shell for class if the above is not working
  * [Coding Ground](https://www.tutorialspoint.com/codingground.htm) lists several tools of interest including C, C++, and CentOS for an online shell
  * Show OSX shell that I use

# Connecting to the Department Machines (SSH) 

`ssh schizo.cs.byu.edu` (need to teach what program to use for the terminal)

  * **OSX**: `Terminal`
  * **Windows**: [PuTTy](https://www.putty.org/) (super easy--use Port 22 for **ssh**), [Git for Windows](https://gitforwindows.org/) with `Git Bash` (fairly easy), or Windows Linux Subsystem with `bash` (less easy).
  * Linux Ubuntu: `Terminal Emulator`

`schizo.cs.byu.edu` is a group of machines on the CS network that you can *connect* with via the Internet.

** Always work on the CS machines.** It will save you hours of headache. 

## VPN

The Bomb and Attack labs require that you connect to the department VPN to download materials. All the instructions can be found by Googling *BYU CS VPN*.

Important theme in this course: how to use the Internet as a resource for C, Linux, and the command line. The VPN makes it so you look like you are on the CS network; it means that all your connections come from the CS network even if you are off campus.

Only the Bomb and Attack labs require you be on the CS network via the VPN and only then to download the materials. Once you have the materials, you should disconnect from the VPN and just use the normal `ssh` to get to `schizo.cs.byu.edu`.

That said, all the labs require that the work be done on `schizo.cs.byu.edu` via `ssh`.

# Navigating the Filesystem

All these have *help* pages that are always available and much easier to understand than sorting through millions of hits on an Internet search. The `man` command, short for *manual*, gives the help page. For example, if you want to learn about `ls`, then at the prompt type `man ls`. Don't be afraid to check the manual page for any command.

  * `pwd` (orientate to the department FS)
  * `ls -al` or `ls` or `ls -ald`: define `.` and `..`
  	
    Need to discuss permissions. What are they and what do they do?
  	Browse around and find a world readable directory.
   
  * `cd` with `cd ~` or `cd` alone to get to home directory show how to move about
  * Draw the directory tree: connect the path to the tree
  * Use the "tree" command to print the directory tree
  * The "man" command

My demo will have colorized listings (unfortunately). There is a switch for `ls` for colors (see `man ls`). I also have an alias setup so that `ls` always includes the needed flag for colors.

**Shortcuts**: the tab key will auto-complete what you are typing and the up/down arrow keys recycle prior commands so they do not need to be re-typed.

# Creating and deleting files

Use the manual pages to learn more about each of these commands: `man <cmd>` as in `man touch`.

  * `touch`: creates a file
  * `nano`: edits a file
  * `mkdir`: creates a directory
  * `rm`: deletes a file
  * `rm -rf`: deletes a directory (be careful)
  * `mv`: moves a file
  * `cp`: copies a file
  * `scp`: copies a file from one machine to another machine (similar to `ssh` but for copying files)

## Class Activity

  * Create a random directory structure and files.
  * Point Finder or Windows explorer to this subdirectory and expand all levels: **open .**
  * Ask students to recreate the tree with the commands they have learned

# Compiling Programs

The compiler is `gcc` for this course. **Always use gcc on a lab machine in a terminal via ssh**. It will save you hours of time and avoid grading surprises.

  * Compiling a program: `gcc -Wall <file>.c` (creates on executable name `a.out` from the source in `<file>.c`, the `-Wall` turns on all warnings---**always fix all warnings**)
  * Giving a name other than `a.out`: `gcc -Wall <file>.c -o <file>` (creates an executable named `<file>`)
  * Compiling with debug information: `gcc -Wall -g <file>.c`

Running a program is easy from the command prompt: `./a.out`.

The preceding `./` directs the shell to look in the current directory to find the executable named `a.out`. The current directory can be added to the search path so that the `./` is not required by updating the `PATH` environment variable. It is not required in this course and is considered an *advanced* topic although it is not at all hard to do.

# Standard In and Standard Out

*Standard in* is where keyboard input comes from and is referred to as `stdin`. *Standard out* is where program output goes from `cout` and is referred to as `stdout`.

  * `cat` to write file to `stdout`
  * `echo` to write any text to `stdout`
  * Redirect `stdout` to a file with `>`
  * Redirect `stdout` to `stdin` with `|`
  * Use grep as an example: `ls | grep` or `cat | grep`
  * Redirect a file to `stdin` with `<` as in `grep < file.txt`
  
# Job control

  * **CTRL-d** (^d) signals the end of `stdin` input
  * **CTRL-c** (^c) kills a program and is very important (never leave programs running when leaving a machine)
  * **CTRL-z** (^z) suspend or stop a program
  * **fg** un-suspend or start a program

# Debugging with GDB

See the [primer](https://bitbucket.org/byucs224/byu-cs-224-labs/src/master/gdb-primer.md).
