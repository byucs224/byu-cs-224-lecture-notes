# Objective: 
  * Visualize how arrays are stored in memory in a contiguous block
  * Demonstrate why overwriting an array bound corrupts other memory

# ssh, gcc, and ctrl-c

 * Use a terminal to connect to *schizo.cs.byu.edu*
 * Use *who* to show that others are an the machine
 * Compile overflow.c from the command line
 * Add an infinite loop and demonstrate ctrl-c
 * Talk about the *command not found* message and adding ./ when needed
   
# Memory and Arrays
The goal is to arrive at *overflow.c* and have students be able to explain its behavior given certain input. Along the way, show how to read input from a file using pipes.

 1. Create a program that initializes a string with an array initializer and draw on the board what is going on in memory showing the Hex values of the ASCII characters. 
 2. Use a typecast to print out the characters in their hex-format and explain the correlation
 3. Read a string input into that array with **scanf** give fewer characters than the actual size to show the null character
 4. Draw the state of memory on the board
 5. Repeat with a string longer than memory. What happens?

Open *overflow.c* compile and run it with no input. Have students study it and ask what happens on a given input. Students should discuss and write down best guess. Run to see what happens. Discuss the outcome and draw the memory contents on the board.

Mapping memory to the C code is an major outcome for the course and we will spending a lot of time in it.

# Expectation

People are able to use *ssh* to connect to *schizo.cs.byu.edu*, edit files, and compile and run programs.

