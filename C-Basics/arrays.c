#include <stdio.h>

int main(int argc, char* argv[]) {
    int array[4];
    
    printf("sizeof(int *) = %ld\n", sizeof(int *));
    printf("sizeof(int) = %ld\n", sizeof(int));
    printf("array = %p\n", array);
    
    /* Read in data as hexadecimal */
    scanf("%x %x %x %x", array, array + 1, array + 2, array + 3);
    
    /* Write out as integers in hexadecimal */
    printf("array[0] = 0x%x\n", *array);
    printf("array[1] = 0x%x\n", *(array +1));
    printf("array[2] = 0x%x\n", *(array +2));
    printf("array[3] = 0x%x\n", *(array +3));
    
    /* Interpret as an array of characters */
    printf("*((char *)array + 0) = %c\n", *((char *)array + 0));
    printf("*((char *)array + 1) = %c\n", *((char *)array + 1));
    printf("*((char *)array + 2) = %c\n", *((char *)array + 2));
    printf("*((char *)array + 3) = %c\n", *((char *)array + 3));
    
    /* Interpret as a string */
    printf("(char *)array = %s\n", (char *)array);
    
    /* Interpret as a long int */
    printf("*((long int *)array + 0) = 0x%lx\n", *((long int *)array + 0));
    printf("*((long int *)array + 1) = 0x%lx\n", *((long int *)array + 1));
    
    return 0;
}
