#include <stdio.h>

#define SIZE 24

int main() {
  char str[SIZE];
  unsigned *u_ptr = (unsigned *)str;
  unsigned *u_end = u_ptr + SIZE/sizeof(unsigned);

  printf("Type input:\n");
  scanf("%x %x %x %x %x %x", u_ptr, u_ptr + 1, u_ptr + 2, u_ptr + 3, u_ptr + 4, u_ptr + 5);
  printf("Thank you.\n");
  
  while (u_ptr < u_end) {
    printf("u_ptr = %p\t *u_ptr = 0x%x\n", u_ptr, *u_ptr);
    ++u_ptr;
  }

  printf("str = %s\n", str);
  return 0;
}
