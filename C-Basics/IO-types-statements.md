Prefer to use the compile-on-line shells:

  * [Compile C++ Online](https://www.tutorialspoint.com/compile_cpp_online.php)
  * [Compile C Online](https://www.tutorialspoint.com/compile_c_online.php)

If there are issues with the online tools, then default to **Terminal**. Split the screen to show side-by-side if possible.

# stdout and Basic Types

  *   **cat** and **echo** write to **stdout**
  *   **cout** is how to write to **stdout** from C++
  *   map **cout** to **printf**
  *   map **endl** to **\\n**
  *   **Class Activity**: write a **printf** statement to output (including the quotes) "I'm 'diggin' printf". The [Escape Sequences in C](https://en.wikipedia.org/wiki/Escape_sequences_in_C) will be helpful
  *   Paste and run *output.cc* in  [Compile C++ Online](https://www.tutorialspoint.com/compile_cpp_online.php)
  *   Implement the character output with **%c** in *output.c* in [Compile C Online](https://www.tutorialspoint.com/compile_c_online.php)

  *   Implement the integer output with **%d**
  *   **Class Activity**: implement the output for the rest of the types. The [printf format string Type Field](https://en.wikipedia.org/wiki/Printf_format_string#Type_field) will be helpful
  *   Show example that uses multiple format strings to output several variables
  *   **Class Activity**: try a few on your own

# stdin

Use the stdin tab on the online compilers for the input. Type in the input that is needed and then execute.

  *   Compile and run **input.cc**
  *   map **cin** to **scanf** for integers and characters and explain the **&**-operator
  *   **Class Activity**: get input for the other types (there are many ways for **float** and **double** but choose one and use the *l* length-specifier for the **double**
  *   Show how to connect the terminal to **stdin** with | using **cat** and **echo**
  *   Create the file with **echo** using the \> to redirect **stdout** to a file
  *   **Class Activity**: play with pipes

# Statements

Most all of what exited it C++ is in C in terms of statements. **Class Activity**: write a program that reads in two integers and then multiplies them by adding the larger value to itself the smaller value's number of times \(multiply via addition\). Output the inputs and answer.

# Strings
C++ made strings easy and obvious.
```cpp
#include <iostream>
#include <string>

using namespace std;

int main() {
    string str = "Hello strings";
    cout << "str = " << str << endl;
}
```
Look at the C equivalent.

```c
#include <stdio.h>

int main()
{
    char str[] = "Hello strings";
    printf("str = %s\n", str);
    return 0;
}
```

Why is a string a pointer? What does that even mean?
