# Learning Outcomes:

  * Identify the number of bytes used for different types
  * Draw memory with its contents and be able to map memory to C declarations and statemensts
  * Use typecasting to interpret the same memory locations in different ways
  * Given the contents of memory, show how it is interpreted based on type
  * Use typecasting to interpret memory in different ways
  * Get data from the command-line with command-line arguments
  
# overflow.c
 
Review carefully *overflow.c* paying particular attention to

  * The declaration of both arrays and where the compiler places them in memory
  * The typecast of the integer array in the **scanf** call
  * The output of each memory location (print each byte in hex and reprint each as unsigned again in hex)
  
Draw memory with its contents in the more compact four-bytes per line form. Connect what the program is 
doing with what is drawn on the board. Use the **sizeof** call to show the effect of typecasting an the 
number of bytes in memory considered as part of the type. Use pointer arithmatic to print out each location.

Teach the use of redirects to create files, *echo '123 23' > f.txt*, and pipes to put data into **stdin**, *cat f.txt | a.out*.

### Class activity

Write a program that creates an array of 24 characters, but fills the array with **unsigned** integers input from the keyboard 
in hexadecimal. 

  * Print out the array as ASCII characters, one character at a time using a pointer and pointer arithmetic
  * Print out the array as **unsigned** types using a pointer with pointer arithmetic 
  * Repeat the above again only this time as **long unsigned** types
  * Draw memory with its contents and correlate it to the output from the program
  
Are you able to input the right **unsigned** integers to get character output to be, *"CS 224 is mind bending!"*?

A partial solution is in *pointers.c* and *pointers.input*. Resist the urge to look at it until you have figured it out
an your own.

# Optional for arrays and pointers: Command-line Arguments

Create a new program that takes command-line arguments and prints out those arguments to *stdout*.

  

  
