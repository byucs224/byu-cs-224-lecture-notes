#include <iostream>

using namespace std;

int main() {
    char c = 'a';
    int i = -10;
    int* ptr_i = &i;
    unsigned u = 2;
    float f = 2.14;
    double d = f + 0.4;

    cin >> c;
    cout << "c = " << c << endl;
    
    cin >> i;
    cout << "i = " << i << endl;
    cout << "ptr_i = " << ptr_i << endl;

    cin >> u;
    cout << "u = " << u << endl;

    cin >> f;
    cout << "f = " << f << endl;

    cin >> d;
    cout << "d = " << d << endl;

    return 0;
}
