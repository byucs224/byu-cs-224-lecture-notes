#include <iostream>

using namespace std;

int main() {
    char c = 'a';
    int i = -10;
    int* ptr_i = &i;
    unsigned u = 2;
    float f = 2.14;
    double d = f + 0.4;

    cout << "c = " << c << endl;
    cout << "i = " << i << endl;
    cout << "ptr_i = " << ptr_i << endl;
    cout << "u = " << u << endl;
    cout << "f = " << f << endl;
    cout << "d = " << d << endl;

    return 0;
}
